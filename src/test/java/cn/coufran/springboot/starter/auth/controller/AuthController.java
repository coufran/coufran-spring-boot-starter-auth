package cn.coufran.springboot.starter.auth.controller;

import cn.coufran.springboot.starter.auth.AuthUser;
import cn.coufran.springboot.starter.auth.Certificate;
import cn.coufran.springboot.starter.auth.annotation.Public;
import cn.coufran.springboot.starter.auth.bean.User;
import cn.coufran.springboot.starter.auth.impl.token.TokenCertificate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;

/**
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@RestController
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    @Resource
    private AuthUser<User> authUser;

    @RequestMapping("/login")
    @Public
    public Certificate login() {
        Certificate token = authUser.login(new User(new Random().nextInt()));
        LOGGER.info("login:{}", token);
        return token;
    }

    @RequestMapping("/test")
    public User test() {
        User user = authUser.getData();
        LOGGER.info("test:{}", user);
        return user;
    }

    @RequestMapping("/open")
    public User open() {
        User user = authUser.getData();
        LOGGER.info("open:{}", user);
        return user;
    }


    @RequestMapping("/refresh")
    @Public
    public Certificate refresh() {
        return authUser.refresh();
    }

    @RequestMapping("/logout")
    @Public
    public void logout() {
        authUser.logout();
    }
}
