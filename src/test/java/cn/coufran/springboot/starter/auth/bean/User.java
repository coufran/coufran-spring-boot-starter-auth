package cn.coufran.springboot.starter.auth.bean;

/**
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class User {
    private Integer id;

    public User(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                '}';
    }
}
