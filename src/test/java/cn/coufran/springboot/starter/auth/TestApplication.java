package cn.coufran.springboot.starter.auth;

import cn.coufran.springboot.starter.auth.impl.session.config.EnableAuthSession;
import cn.coufran.springboot.starter.auth.impl.token.config.EnableAuthToken;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@SpringBootApplication
@EnableAuthToken
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
