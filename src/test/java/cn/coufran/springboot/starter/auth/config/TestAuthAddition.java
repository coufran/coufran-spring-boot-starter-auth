package cn.coufran.springboot.starter.auth.config;

import cn.coufran.springboot.starter.auth.AuthUser;
import cn.coufran.springboot.starter.auth.bean.User;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
//@Component
public class TestAuthAddition implements AuthAddition<User> {
    @Override
    public void authIntercept(AuthUser<User> authUser, HttpServletRequest request, Object handler) throws AuthException {
        throw new AuthException("测试错误");
    }
}
