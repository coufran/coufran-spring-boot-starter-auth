package cn.coufran.springboot.starter.auth.impl.session.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用Session验证
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(SessionAuthConfig.class)
public @interface EnableAuthSession {

}
