/**
 * 认证拦截的简单实现
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
package cn.coufran.springboot.starter.auth.impl.simple;