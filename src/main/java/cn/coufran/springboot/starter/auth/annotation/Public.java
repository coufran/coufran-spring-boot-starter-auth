package cn.coufran.springboot.starter.auth.annotation;

import java.lang.annotation.*;

/**
 * 标注开放接口
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Public {
}
