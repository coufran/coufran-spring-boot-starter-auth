package cn.coufran.springboot.starter.auth.impl.token.config;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用Token验证
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(TokenAuthConfig.class)
public @interface EnableAuthToken {
}
