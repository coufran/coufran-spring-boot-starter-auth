package cn.coufran.springboot.starter.auth.impl.token;

import cn.coufran.springboot.starter.auth.Certificate;

/**
 * Token证书
 * @author Coufran
 * @version 1.1.0
 * @since 1.0.0
 */
public class TokenCertificate implements Certificate {
    /** Token */
    private String accessToken;
    /** Token有效时间（秒） */
    private int expireTime;
    /** Refresh Token */
    private String refreshToken;
    /** Refresh Token有效时间（秒） */
    private int refreshExpireTime;

    /**
     * 构造Token证书
     * @param accessToken Token
     */
    public TokenCertificate(String accessToken) {
        this.accessToken = accessToken;
    }

    /**
     * 构造Token证书
     * @param accessToken Token
     * @param expireTime Token有效时间
     */
    public TokenCertificate(String accessToken, int expireTime) {
        this.accessToken = accessToken;
        this.expireTime = expireTime;
    }

    /**
     * 构造Token证书
     * @param accessToken Token
     * @param expireTime Token有效时间
     * @param refreshToken RefreshToken
     * @param refreshExpireTime RefreshToken有效时间
     */
    public TokenCertificate(String accessToken, int expireTime, String refreshToken, int refreshExpireTime) {
        this.accessToken = accessToken;
        this.expireTime = expireTime;
        this.refreshToken = refreshToken;
        this.refreshExpireTime = refreshExpireTime;
    }

    /**
     * 获取Token
     * @return Token
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * 获取Token有效时间
     * @return Token有效时间
     */
    public int getExpireTime() {
        return expireTime;
    }

    /**
     * 获取RefreshToken
     * @return RefreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * 获取RefreshToken有效时间
     * @return RefreshToken有效时间
     */
    public int getRefreshExpireTime() {
        return refreshExpireTime;
    }
}
