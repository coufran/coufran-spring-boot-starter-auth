package cn.coufran.springboot.starter.auth.config;

import cn.coufran.springboot.starter.auth.AuthUser;
import cn.coufran.springboot.starter.auth.impl.simple.SimpleAuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.context.annotation.RequestScope;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限配置
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@Import(SpringWebMvcConfig.class)
public class AuthConfig {
    /** 拦截路径白名单配置 */
    @Value("${coufran.auth.whitePath:}")
    private List<String> whitePaths;

    /** 自定义拦截规则 */
    @Autowired(required = false)
    private List<AuthAddition> authAdditions = new ArrayList<>();

    /**
     * 默认权限用户
     * @return 默认权限用户
     */
    @Bean
    @RequestScope
    @ConditionalOnMissingBean(AuthUser.class)
    public AuthUser authUser() {
        return new SimpleAuthUser<>();
    }

    /**
     * 权限拦截器
     * @param authUser 权限用户
     * @return 权限拦截器
     */
    @Bean
    public AuthInterceptor authInterceptor(AuthUser authUser) {
        AuthInterceptor authInterceptor = new AuthInterceptor(authUser);
        authInterceptor.setWhitePaths(whitePaths);
        authInterceptor.setAuthAdditions(authAdditions);
        return authInterceptor;
    }
}
