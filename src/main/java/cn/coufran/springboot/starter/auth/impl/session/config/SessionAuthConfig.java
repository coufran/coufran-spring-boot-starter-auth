package cn.coufran.springboot.starter.auth.impl.session.config;

import cn.coufran.springboot.starter.auth.AuthUser;
import cn.coufran.springboot.starter.auth.AuthUserManager;
import cn.coufran.springboot.starter.auth.impl.session.SessionAuthUserManager;
import cn.coufran.springboot.starter.auth.impl.session.SessionCertificate;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.RequestScope;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Session权限拦截配置
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class SessionAuthConfig {
    /**
     * 权限上下文
     * @param session Session对象
     * @return 权限上下文
     */
    @Bean
    public AuthUserManager authUserManager(HttpSession session) {
        return new SessionAuthUserManager(session);
    }

    /**
     * 权限用户
     * @param request Request对象
     * @param authUserManager 权限上下文
     * @param <U> 权限用户承载的数据类型
     * @return 权限用户
     */
    @Bean
    @RequestScope
    public <U> AuthUser<U> authUser(HttpServletRequest request, AuthUserManager authUserManager) {
        return authUserManager.getAuthUser(new SessionCertificate());
    }

}
