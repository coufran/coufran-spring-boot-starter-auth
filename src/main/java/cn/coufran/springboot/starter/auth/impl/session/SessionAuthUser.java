package cn.coufran.springboot.starter.auth.impl.session;

import cn.coufran.springboot.starter.auth.AbstractAuthUser;

/**
 * 基于Session的权限用户
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class SessionAuthUser<D> extends AbstractAuthUser<D> {
    /**
     * 构造权限用户
     * @param authUserManager 权限上下文
     */
    public SessionAuthUser(SessionAuthUserManager authUserManager) {
        super(authUserManager);
    }

    /**
     * 构造权限用户
     * @param authUserManager 权限上下文
     * @param data 数据
     */
    public SessionAuthUser(SessionAuthUserManager authUserManager, D data) {
        super(authUserManager, data);
    }
}
