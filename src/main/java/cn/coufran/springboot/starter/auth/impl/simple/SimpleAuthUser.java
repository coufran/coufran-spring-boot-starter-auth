package cn.coufran.springboot.starter.auth.impl.simple;

import cn.coufran.springboot.starter.auth.AuthUser;
import cn.coufran.springboot.starter.auth.Certificate;

/**
 * 基本的权限用户实现
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class SimpleAuthUser<D> implements AuthUser<D> {
    /** 是否通过认证 */
    private boolean authenticated;
    /** 数据 */
    private D data;

    /**
     * 是否已认证
     * @return 已认证返回true
     */
    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    /**
     * 获取基础数据
     * @return 基础数据
     */
    @Override
    public D getData() {
        return data;
    }

    /**
     * 登录并设置基础数据
     * @param data 基础数据
     * @return 此次登录对应的Token组
     */
    @Override
    public Certificate login(D data) {
        this.authenticated = true;
        this.data = data;
        return new SimpleCertificate();
    }

    @Override
    public Certificate refresh() {
        return new SimpleCertificate();
    }

    /**
     * 登出
     */
    @Override
    public void logout() {
        this.authenticated = false;
        this.data = null;
    }
}
