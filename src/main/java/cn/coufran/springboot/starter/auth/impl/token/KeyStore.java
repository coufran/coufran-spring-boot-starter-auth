package cn.coufran.springboot.starter.auth.impl.token;

/**
 * 秘钥存储组件
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public interface KeyStore {
    /**
     * 存储秘钥
     * @param key 秘钥
     */
    void save(byte[] key);

    /**
     * 加载秘钥
     * @return 秘钥，无秘钥返回null
     */
    byte[] load();
}
