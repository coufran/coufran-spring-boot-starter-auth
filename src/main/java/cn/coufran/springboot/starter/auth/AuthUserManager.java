package cn.coufran.springboot.starter.auth;

/**
 * 权限上下文，负责登录、登出、状态记录、权限判定等操作
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public interface AuthUserManager {
    /**
     * 获取权限用户
     * @param certificate 证书
     * @param <D> 权限用户基础数据类型
     * @return 权限用户，未登录返回匿名用户
     */
    default <D> AuthUser<D> getAuthUser(Certificate certificate) {
        if (certificate == null) {
            return createAnonymous();
        }
        AuthUser<D> authUser = reloadAuthenticated(certificate);
        if (authUser == null) {
            authUser = createAnonymous();
        }
        return authUser;
    }

    /**
     * 重新加载实名用户
     * @param certificate 证书
     * @param <D> 权限用户基础数据类型
     * @return 权限用户
     */
    <D> AuthUser<D> reloadAuthenticated(Certificate certificate);

    /**
     * 创建匿名用户
     * @param <D> 权限用户基础数据类型
     * @return 匿名用户
     */
    <D> AuthUser<D> createAnonymous();

    /**
     * 权限用户登录上下文，并返回证书
     * @param authUser 权限用户
     * @param data 基础数据
     * @param <D> 权限用户基础数据类型
     * @return 证书
     */
    <D> Certificate acceptLogin(AuthUser<D> authUser, D data);

    /**
     * 刷新权限用户
     * @param authUser 权限用户
     * @param <D> 权限用户基础数据类型
     * @return 新的权限证书
     */
    <D> Certificate acceptRefresh(AuthUser<D> authUser);

    /**
     * 权限用户登出上下文
     * @param authUser 权限用户
     * @param <U> 权限用户基础数据类型
     */
    <U> void acceptLogout(AuthUser<U> authUser);
}