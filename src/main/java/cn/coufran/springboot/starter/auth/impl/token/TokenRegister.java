package cn.coufran.springboot.starter.auth.impl.token;

/**
 * Token注册机，实现Token的生成、校验和解析
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public interface TokenRegister {
    /**
     * 生成Token
     * @param payload Token负载
     * @param expireTime 有效
     * @return Token
     */
    String generate(String payload, int expireTime);

    /**
     * 校验并解析Token
     * @param token Token
     * @return Token负载，解析失败或Token过期等异常返回null
     */
    String parse(String token);
}
