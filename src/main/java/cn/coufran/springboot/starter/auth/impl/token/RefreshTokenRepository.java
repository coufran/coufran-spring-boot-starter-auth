package cn.coufran.springboot.starter.auth.impl.token;

import java.util.Date;

/**
 * Refresh Token存储
 * @author Coufran
 * @version 1.1.0
 * @since 1.1.0
 */
public interface RefreshTokenRepository {
    /**
     * 保存Refresh Token（原子操作）
     * @param refreshToken refresh token
     * @param expireTime 过期时间
     */
    void save(String refreshToken, Date expireTime);

    /**
     * 删除并返回是否存在Refresh Token（原子操作）
     * @param refreshToken refresh token
     * @return 存在返回true
     */
    boolean delete(String refreshToken);
}
