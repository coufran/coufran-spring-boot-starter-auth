package cn.coufran.springboot.starter.auth.impl.token;

import cn.coufran.springboot.starter.auth.AbstractAuthUser;

/**
 * 基于Token的权限用户
 * @param <D> 数据类型
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class TokenAuthUser<D> extends AbstractAuthUser<D> {
    /** 权限 */
    private Auth auth;
    /** Token原始值 */
    private String token;

    /**
     * 获取权限
     * @return 获取权限
     */
    public Auth getAuth() {
        return auth;
    }

    /**
     * 获取Token原始值
     * @return Token原始值
     */
    public String getToken() {
        return token;
    }

    /**
     * 是否已认证
     * @return 已认证返回true
     */
    @Override
    public boolean isAuthenticated() {
        return super.isAuthenticated() && auth == Auth.ACCESS;
    }

    /**
     * 构造权限用户
     * @param authUserManager 权限上下文
     */
    public TokenAuthUser(TokenAuthUserManager authUserManager) {
        super(authUserManager);
    }

    /**
     * 构造权限用户
     * @param authUserManager 权限上下文
     * @param data 数据
     * @param auth 拥有的权限
     * @param token Token
     */
    public TokenAuthUser(TokenAuthUserManager authUserManager, D data, Auth auth, String token) {
        super(authUserManager, data);
        this.auth = auth;
        this.token = token;
    }

    /**
     * 不支持登出，所以登出直接返回
     */
    @Override
    public void logout() {
    }

    /**
     * 权限
     * @author Coufran
     * @since 1.1.0
     * @version 1.1.0
     */
    public enum Auth {
        /** 访问系统 */
        ACCESS,
        /** 刷新Token */
        REFRESH
    }
}
