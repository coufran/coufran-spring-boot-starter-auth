package cn.coufran.springboot.starter.auth.impl.session;

import cn.coufran.springboot.starter.auth.Certificate;

/**
 * 空证书
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class SessionCertificate implements Certificate {
}
