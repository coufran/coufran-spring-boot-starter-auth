package cn.coufran.springboot.starter.auth.impl.token;

/**
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class DummyKeyStore implements KeyStore {
    @Override
    public void save(byte[] key) {
    }

    @Override
    public byte[] load() {
        return null;
    }
}
