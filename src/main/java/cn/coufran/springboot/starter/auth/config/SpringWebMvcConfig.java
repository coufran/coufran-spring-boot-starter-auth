package cn.coufran.springboot.starter.auth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * Spring MVC配置
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
public class SpringWebMvcConfig implements WebMvcConfigurer {
    /** 权限拦截器 */
    private AuthInterceptor authInterceptor;

    /**
     * 注入权限拦截器
     * @param authInterceptor 权限拦截器
     */
    @Resource
    public void setAuthInterceptor(AuthInterceptor authInterceptor) {
        this.authInterceptor = authInterceptor;
    }

    /**
     * 添加权限拦截器
     * @param registry 权限拦截器
     */
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor);
    }
}
