package cn.coufran.springboot.starter.auth.impl.token;

/**
 * Token Payload转换器，实现数据和Payload字符串的互相转换
 * @author Coufran
 * @version 1.1.0
 * @since 1.1.0
 */
public interface TokenPayloadConverter<T> {
    /**
     * 转换器名称，越短越好，不得与已有的重复，不可以包含“|”
     * @return 转换器名称
     */
    String getName();

    /**
     * 是否支持转换指定数据
     * @param data 指定的数据
     * @return 支持返回true
     */
    boolean support(T data);

    /**
     * 数据转换为Payload
     * @param data 数据
     * @return Payload
     */
    String serialize (T data);

    /**
     * Payload转换为数据
     * @param payload Payload
     * @param <D> 数据类型
     * @return 数据
     */
    <D> D deserialize(String payload);
}
