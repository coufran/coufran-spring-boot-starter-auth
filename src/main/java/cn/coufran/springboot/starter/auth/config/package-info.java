/**
 * 权限拦截相关配置
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
package cn.coufran.springboot.starter.auth.config;