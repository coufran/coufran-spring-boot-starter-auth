package cn.coufran.springboot.starter.auth.impl.token;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 基于JSON的POJO payload转换器，序列化规则：<br>
 * ClassName|JSON数据
 * @author Coufran
 * @version 1.1.0
 * @since 1.1.0
 */
public class PojoJsonTokenPayloadConverter implements TokenPayloadConverter<Object> {
    /** Token负载解析Pattern */
    private static final Pattern PATTERN_PAYLOAD = Pattern.compile("(.+)\\|(.+)");

    /**
     * 转换器名称：PojoJson
     * @return 转换器名称
     */
    @Override
    public String getName() {
        return "PojoJson";
    }

    /**
     * 是否支持转换指定数据
     * @param data 指定的数据
     * @return 支持返回true
     */
    @Override
    public boolean support(Object data) {
        return true;
    }

    /**
     * 数据转换为Payload
     * @param data 数据
     * @return Payload
     */
    @Override
    public String serialize(Object data) {
        String dataJson = JSON.toJSONString(data);
        String className = data.getClass().getName();
        return className + "|" + dataJson;
    }

    /**
     * Payload转换为数据
     * @param payload Payload
     * @param <D> 数据类型
     * @return 数据
     */
    @Override
    public <D> D deserialize(String payload) {
        Matcher payloadMatcher = PATTERN_PAYLOAD.matcher(payload);
        if (!payloadMatcher.matches()) {
            return null;
        }
        Class<?> dataClass;
        try {
            String className = payloadMatcher.group(1);
            dataClass = Class.forName(className);
        } catch (ClassNotFoundException e) {
            return null;
        }
        String dataJson = payloadMatcher.group(2);
        try {
            return (D) JSON.parseObject(dataJson, dataClass);
        } catch (JSONException e) {
            return null;
        }
    }

}
