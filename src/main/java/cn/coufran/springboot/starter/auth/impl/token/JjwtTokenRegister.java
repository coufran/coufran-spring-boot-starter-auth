package cn.coufran.springboot.starter.auth.impl.token;

import cn.coufran.commons.exception.ServiceException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * 基于Jjwt的Token注册机，重启后秘钥重置，所有Token失效
 * @author Coufran
 * @version 1.0.0
 * @since 1.0.0
 */
public class JjwtTokenRegister implements TokenRegister {
    /** Jjwt秘钥 */
    private SecretKey KEY;

    /**
     * 构造注册机，生成秘钥
     */
    public JjwtTokenRegister() {
        try {
            KEY = KeyGenerator.getInstance("HmacSHA256").generateKey();
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * 给定秘钥，构造注册机
     * @param key
     */
    public JjwtTokenRegister(byte[] key) {
        KEY = new SecretKeySpec(key, "HmacSHA256");
    }

    /**
     * 获取秘钥
     * @return 秘钥
     */
    public byte[] getKey() {
        return KEY.getEncoded();
    }

    /**
     * 生成Token
     * @param payload Token负载
     * @param expireTime 有效
     * @return Token
     */
    @Override
    public String generate(String payload, int expireTime) {
        HashMap<String, Object> claims = new HashMap<>();
        claims.put("data", payload);
        Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + expireTime * 60 * 1000))
                .setId(UUID.randomUUID().toString())
                .signWith(SignatureAlgorithm.HS256, KEY)
                .compact();
    }

    /**
     * 校验并解析Token
     * @param token Token
     * @return Token负载，解析失败或Token过期等异常返回null
     */
    @Override
    public String parse(String token) {
        try {
            return Jwts.parser()
                    .setSigningKey(KEY)
                    .parseClaimsJws(token)
                    .getBody()
                    .get("data", String.class);
        } catch (JwtException e) {
            return null;
        }
    }
}
