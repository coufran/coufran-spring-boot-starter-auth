package cn.coufran.springboot.starter.auth;

/**
 * 权限用户
 * @param <D> 权限用户基础数据
 * @author Coufran
 * @version 1.1.0
 * @since 1.0.0
 */
public interface AuthUser<D> {
    /**
     * 是否已认证
     * @return 已认证返回true
     */
    boolean isAuthenticated();

    /**
     * 获取基础数据
     * @return 基础数据
     */
    D getData();

    /**
     * 登录并设置基础数据
     * @param data 基础数据
     * @return 权限证书
     */
    Certificate login(D data);

    /**
     * 刷新登录状态
     * @return 新权限证书
     */
    Certificate refresh();

    /**
     * 登出
     */
    void logout();
}
